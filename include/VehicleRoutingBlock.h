/*--------------------------------------------------------------------------*/
/*----------------------- File VehicleRoutingBlock.h -----------------------*/
/*--------------------------------------------------------------------------*/
/** @file
 * ....
 *
 * \version 0.1
 *
 * \date 10 - 05 - 2021
 *
 * \author Antonio Frangioni \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 * \author Ali Ghezelsoflu \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * Copyright &copy by Antonio Frangioni, Ali Ghezelsoflu
 */
/*--------------------------------------------------------------------------*/
/*----------------------------- DEFINITIONS --------------------------------*/
/*--------------------------------------------------------------------------*/

#ifndef __VehicleRoutingBlock
#define __VehicleRoutingBlock
/* self-identification: #endif at the end of the file */

/*--------------------------------------------------------------------------*/
/*------------------------------ INCLUDES ----------------------------------*/
/*--------------------------------------------------------------------------*/

#include "Block.h"
#include "ColVariable.h"

/*--------------------------------------------------------------------------*/
/*------------------------------ NAMESPACE ---------------------------------*/
/*--------------------------------------------------------------------------*/

/// Namespace for the Structured Modeling System++ (SMS++)

namespace SMSpp_di_unipi_it {

/*--------------------------------------------------------------------------*/
/*----------------------- CLASS VehicleRoutingBlock ------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------- GENERAL NOTES --------------------------------*/
/*--------------------------------------------------------------------------*/
/// implementation of the Block concept for a "generic VR problem"
/** The class VehicleRoutingBlock, which derives from the Block, defines a
 * base class for any possible "VR problem" that can be attached to a
 * MultiVehicleRoutingBlock.
 * */

class VehicleRoutingBlock : public Block {

/*--------------------------------------------------------------------------*/
/*----------------------- PUBLIC PART OF THE CLASS -------------------------*/
/*--------------------------------------------------------------------------*/

 public:

/*--------------------------------------------------------------------------*/
/*---------------------- PUBLIC TYPES OF THE CLASS -------------------------*/
/*--------------------------------------------------------------------------*/

//TODO

/*--------------------------------------------------------------------------*/
/*--------------------- CONSTRUCTOR AND DESTRUCTOR -------------------------*/
/*--------------------------------------------------------------------------*/
/** @name constructor and destructor
 *  @{ */

/// constructor, takes the father
/** Constructor of VehicleRoutingBlock, taking possibly a pointer of its
 * father Block.
 * */

 explicit VehicleRoutingBlock( Block * father = nullptr ) : Block( father )

/*--------------------------------------------------------------------------*/
/// destructor of VehicleRoutingBlock: deletes the abstract representation
 virtual ~VehicleRoutingBlock() override {
  for( auto & block : v_Block )
   delete block;
  v_Block.clear();
 }
/**@} ----------------------------------------------------------------------*/
/*-------------------------- OTHER INITIALIZATIONS -------------------------*/
/*--------------------------------------------------------------------------*/
/** @name Other initializations
 *  @{ */

/// extends Block::deserialize( netCDF::NcGroup )
/** Extends Block::deserialize( netCDF::NcGroup ) to the specific format of
 * the TwoSlotsVRPBlock. Besides what is managed by serialize() method of the
 * base Block class, the group should contains the following:
 *
 * - the dimension "NumberImporters" containing the number of Importers.
 *
 * - the dimension "NumberExporters" containing the number of Exporters. A
 *   value that is useful in the following is the "NumberCustomers". This is
 *   computed by just calling get_number_NumberImporters() and
 *   get_number_NumberExporters(), then summing all the results.
 *
 * - the dimension "TruckTypes" containing the number of different types of
 *   available trucks. This dimension is optional, if it is not provided then
 *   it is taken to be == 1.
 *
 *   */

 void deserialize( const netCDF::NcGroup & group ) override;


/**@} ----------------------------------------------------------------------*/
/*-------- METHODS FOR READING THE DATA OF THE VehicleRoutingBlock ---------*/
/*--------------------------------------------------------------------------*/
 /// returns the number of importers
 Index get_number_importers() const { return f_number_importers; }

 /// returns the number of exporters
 Index get_number_exporters() const { return f_number_exporters; }

 /// returns the type of trucks
 Index get_truck_types() const { return f_truck_types; }

/**@} ----------------------------------------------------------------------*/
/*------- METHODS FOR READING THE Variable OF THE VehicleRoutingBlock ------*/
/*--------------------------------------------------------------------------*/
/** @name Reading the Variable of the VehicleRoutingBlock
 * These methods allow to read the each group of Variable that any
 * VehicleRoutingBlock in principle has:
 *
 *  - the integer decision variables;
 *
 *  - the slack tardiness variables;
 *
 *  - the slack earliness variables;
 * @{ */

 /// returns the vector of integer decision variables
 virtual ColVariable * get_integer_decision( ) {
  return ( nullptr);
 }
/*--------------------------------------------------------------------------*/
 /// returns the matrix of slack tardiness variable
 virtual ColVariable * get_slack_tardiness( ) {
  return( nullptr );
 }
/*--------------------------------------------------------------------------*/
 /// returns the matrix of slack earliness variable
 virtual ColVariable * get_slack_earliness( ) {
  return( nullptr );
 }

/**@} ----------------------------------------------------------------------*/
/*----------------------- Methods for handling Solution --------------------*/
/*--------------------------------------------------------------------------*/

//todo if any

/**@} ----------------------------------------------------------------------*/
/*--------------- METHODS FOR SAVING THE VehicleRoutingBlock----------------*/
/*--------------------------------------------------------------------------*/
/** @name Methods for loading, printing & saving the VehicleRoutingBlock
 *  @{ */

/// extends Block::serialize( netCDF::NcGroup )
/** Extends Block::serialize( netCDF::NcGroup ) to the specific format of a
 *
 * */

 void serialize( netCDF::NcGroup & group ) const override;

/**@} ----------------------------------------------------------------------*/
/*------------ METHODS FOR INITIALIZING THE VehicleRoutingBlock ------------*/
/*--------------------------------------------------------------------------*/

/** @name Handling the data of the VehicleRoutingBlock
    @{ */

 void load( std::istream & input ) override {
  throw ( std::logic_error( "VehicleRoutingBlock::load() not "
                                                        "implemented yet") );
 };

/*--------------------------------------------------------------------------*/
/*-------------------- PROTECTED PART OF THE CLASS -------------------------*/
/*--------------------------------------------------------------------------*/

 protected:

/*--------------------------------------------------------------------------*/
/*-------------------- PROTECTED FIELDS OF THE CLASS -----------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------data--------------------------------------*/
 /// the number of importers
 Index f_number_importers;

 /// the number of exporters
 Index f_number_exporters;

 /// the truck types
 Index f_truck_types;

/*-----------------------------variables------------------------------------*/

/*--------------------------------------------------------------------------*/
/*----------------------- PRIVATE PART OF THE CLASS ------------------------*/
/*--------------------------------------------------------------------------*/
 private:

/*--------------------------------------------------------------------------*/
/*--------------------------- PRIVATE FIELDS -------------------------------*/
/*--------------------------------------------------------------------------*/

 SMSpp_insert_in_factory_h;

/*--------------------------------------------------------------------------*/
/*-------------------------- PRIVATE METHODS -------------------------------*/
/*--------------------------------------------------------------------------*/

};  // end( class( VehicleRoutingBlock ) )

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

}  // end( namespace SMSpp_di_unipi_it )

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

#endif /* VehicleRoutingBlock.h included */

/*--------------------------------------------------------------------------*/
/*------------------- End File VehicleRoutingBlock.h -----------------------*/
/*--------------------------------------------------------------------------*/
