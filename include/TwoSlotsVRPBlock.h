/*--------------------------------------------------------------------------*/
/*------------------------- File TwoSlotsVRPBlock.h ------------------------*/
/*--------------------------------------------------------------------------*/
/** @file
 * Header file for the class TwoSlotsVRPBlock, which represents a multi-period
 * drayage problem in which customers request transportation services over
 * several days, possibly leaving the carrier some flexibility to change
 * service periods.
 *
 * \version 0.1
 *
 * \date 10 - 05 - 2021
 *
 * \author Antonio Frangioni \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 * \author Ali Ghezelsoflu \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * Copyright &copy by Antonio Frangioni, Ali Ghezelsoflu
 */
/*--------------------------------------------------------------------------*/
/*----------------------------- DEFINITIONS --------------------------------*/
/*--------------------------------------------------------------------------*/

#ifndef __TwoSlotsVRPBlock
#define __TwoSlotsVRPBlock
/* self-identification: #endif at the end of the file */

/*--------------------------------------------------------------------------*/
/*------------------------------ INCLUDES ----------------------------------*/
/*--------------------------------------------------------------------------*/

#include "ColVariable.h"
#include "FRowConstraint.h"
#include "FRealObjective.h"


/*--------------------------------------------------------------------------*/
/*------------------------------ NAMESPACE ---------------------------------*/
/*--------------------------------------------------------------------------*/

/// Namespace for the Structured Modeling System++ (SMS++)

namespace SMSpp_di_unipi_it {

/*--------------------------------------------------------------------------*/
/*------------------------ CLASS TwoSlotsVRPBlock --------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------- GENERAL NOTES --------------------------------*/
/*--------------------------------------------------------------------------*/
/// implementation of the Block concept for the TwoSlotsVRPBlock
/** The TwoSlotsVRPBlock class implements the Block concept [see Block.h] of a
 * multi-period drayage problem which contains a path-based model with all
 * feasible routes, a "Price-and-Branch" algorithm in which the pricing is
 * formulated as a collection of shortest path problems in a cunningly
 * constructed acyclic network, and a compact arc-flow formulation based on
 * this network.
 * */

class TwoSlotsVRPBlock : public Block {

/*--------------------------------------------------------------------------*/
/*----------------------- PUBLIC PART OF THE CLASS -------------------------*/
/*--------------------------------------------------------------------------*/

 public:

/*--------------------------------------------------------------------------*/
/*---------------------- PUBLIC TYPES OF THE CLASS -------------------------*/
/*--------------------------------------------------------------------------*/

//TODO

/*--------------------------------------------------------------------------*/
/*--------------------- CONSTRUCTOR AND DESTRUCTOR -------------------------*/
/*--------------------------------------------------------------------------*/
/** @name constructor and destructor
 *  @{ */

/// constructor, takes the ...
/** Constructor of TwoSlotsVRPBlock, .... //TODO
 *
 *
 * */

 explicit TwoSlotsVRPBlock( Block * father = nullptr ) : Block( father ) {}

/*--------------------------------------------------------------------------*/
/// destructor of TwoSlotsVRPBlock: deletes the abstract representation

 virtual ~TwoSlotsVRPBlock();

/**@} ----------------------------------------------------------------------*/
/*-------------------------- OTHER INITIALIZATIONS -------------------------*/
/*--------------------------------------------------------------------------*/
/** @name Other initializations
 *  @{ */

/// extends Block::deserialize( netCDF::NcGroup )
/** Extends Block::deserialize( netCDF::NcGroup ) to the specific format of
 * the TwoSlotsVRPBlock. Besides what is managed by serialize() method of the
 * base Block class, the group should contains the following:
 *
 * - the variable "NumberTrucks" of type netCDF::NcInt and indexed both over
 *   the dimensions "TruckTypes" and "NumberPeriods"; the entry
 *   NumberTrucks[ t , v ] containing the number of trucks of type t which are
 *   available in period h.
 *
 * - the variable "Alpha", of type netCDF::NcInt and indexed both over the
 *   dimensions "NumberCustomers" and "NumberRoutes"; the entry Alpha[ v , r ]
 *   equal to 2 if customer v is served by two container loads carried by a
 *   single (two-container) truck doing route r; or equal to 1 if customer v
 *   is served by one container and 0 if customer v is not visited in route r.
 *
 * - The variable "Cost", of type double and indexed over the dimension
 *   "NumberRoutes": entry Cost[ r ] is assumed to contain the cost of route r.
 *   Note that, in our data, servicing a customer twice with the same truck is
 *   considered to have 0 cost, i.e., there is no cost directly associated
 *   with packing/unpacking operations; however, it would be easy to add this
 *   component to the cost of the route (e.g., by having it as the cost of
 *   self-loops).
 *
 * - The variable "TCost", of type double and indexed both over the dimensions
 *   "NumberPeriods" and "NumberCustomers": entry TCost[ h , v ] is assumed to
 *   contain the penalties that the carrier has to pay for "tardiness" of
 *   service in each period h for each customer v.
 *
 * - The variable "ECost", of type double and indexed both over the dimensions
 *   "NPeriods" and "NumberCustomers": entry TCost[ h , v ] is assumed to
 *   contain the penalties that the carrier has to pay for "earliness" of
 *   service in each period h for each customer v.
 *
 *   Note: that the above penalties should be a positive number and they may
 *   differ from customer to customer, possibly from day to day, and from late
 *   to early delivery/retrieval.
 *
 * - The variable "SlackTardinessBound", of type double and indexed both over
 *   the dimensions "NumberPeriods" and "NumberCustomers": This is meant to
 *   represent the entry SlackTardinessBound[ h , v ] is assumed to contain
 *   the arbitrary bounds on the slack variables of each period h and each
 *   customer v which have not been delivered/collected on time. It can be
 *   simply set to infinity (or "Demand") to impose various conditions arising
 *   from either physical or contractual constraints. This variable is
 *   optional, if it is not provided then it is taken to be simply equal to
 *   zero (in fact eliminating all the slack variables) which can be imposed
 *   to ensure that customers are eventually fully served.
 *    .
 * - The variable "SlackEarlinessBound", of type double and indexed both over
 *   the dimensions "NumberPeriods" and "NumberCustomers": This is meant to
 *   represent the entry SlackEarlinessBound[ h , v ] is assumed to contain
 *   the arbitrary bounds on the slack variables of each period h and each
 *   customer v which have not been delivered/retrieved earlier than agreed
 *   time. It can be simply set to infinity (or "Demand") to impose various
 *   conditions arising from either physical or contractual constraints. This
 *   variable is optional, if it is not provided then it is taken to be simply
 *   equal to zero (in fact eliminating all the slack variables) which can be
 *   imposed to ensure that customers are eventually fully served.
 *
 * - The variable "SlackImporterBound", of type double and indexed both over
 *   the dimensions "NumberPeriods" and "NumberImporters": This is meant to
 *   represent the entry SlackImporterBound[ h , i ] is assumed to contain the
 *   minimum number of "very urgent" deliveries is surely performed of each
 *   period h and each importer i. This bounds may correspond to limits
 *   (either physical or, say, related to insurance issues) on local storage
 *   for containers to be processed in subsequent days.
 *
 * - The variable "SlackExporterBound", of type double and indexed both over
 *   the dimensions "NumberPeriods" and "NumberExporters": This is meant to
 *   represent the entry SlackExporterBound[ h , e ] is assumed to contain the
 *   maximum space that the customer has to temporarily store loads that had
 *   been planned to be carried away of each period h and each exporter e.
 *   This bounds can be used to limit the number of retrievals that are
 *   performed earlier than planned, which may be necessary because the
 *   corresponding loads are not yet ready for being taken away.
 *
 *   */

 void deserialize( const netCDF::NcGroup & group ) override;

/*--------------------------------------------------------------------------*/
/// generate the abstract variables of the TwoSlotsVRPBlock
/** The TwoSlotsVRPBlock class has three different variables which are:
 *
 *  - the integer decision variables with size of f_number_routes representing
 *    the number of times in which route r is traversed;
 *
 *  - the positive boost::multi_array< ColVariable, 2 > slack variables as the
 *    first dimension of f_number_customers and as second dimension of
 *    f_number_periods. They are introduced to model the amount of container
 *    loads that, have not been delivered/collected on time for customer v in
 *    the period h;
 *
 *  - the positive boost::multi_array< ColVariable, 2 > slack variables as the
 *    first dimension of f_number_customers and as second dimension of
 *    f_number_periods. They are introduced to model the amount of container
 *    loads that, have been delivered/retrieved earlier than agreed for
 *    customer v in the period h;
 *
 * */
 void generate_abstract_variables( Configuration * stvv = nullptr  ) override;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/// generate the static constraint of the TwoSlotsVRPBlock
/** Method that generates the static constraint of the TwoSlotsVRPBlock.
 * The Path-based model //TODO
 *
 *   \f[
 *
 *   \f]
 *   */
 void generate_abstract_constraints( Configuration * stcc = nullptr ) override;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/// generate the objective of the TwoSlotsVRPBlock
/** Method that generates the objective of the TwoSlotsVRPBlock.
 *
 * .... //TODO
 * */

 void generate_objective( Configuration * objc = nullptr ) override;
/**@} ----------------------------------------------------------------------*/
/*--------- METHODS FOR READING THE DATA OF THE TwoSlotsVRPBlock -----------*/
/*--------------------------------------------------------------------------*/
/** @name Reading the data of the TwoSlotsVRPBlock
 * These methods allow to read data that are required if they needed.
 *
 * @{ */

/// returns the matrix of Alpha
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ v , r ] gives the Alpha variable for customer v at the route r.
 * */
 const boost::multi_array< Index , 2 > & get_alpha() const {
  return ( v_alpha );
 }
/*--------------------------------------------------------------------------*/
 /// returns the vector of Cost
 /** This method returns the cost of each route r.
  * */
 const std::vector< double > & get_cost() const {
  return v_cost;
 }
/*--------------------------------------------------------------------------*/
/// returns the matrix of TCost
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the TCost variable for period h of each customer v.
 * */
 const boost::multi_array< double , 2 > & get_t_cost() const {
  return ( v_TCost );
 }
/*--------------------------------------------------------------------------*/
/// returns the matrix of ECost
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the ECost variable for period h of each customer v.
 * */
 const boost::multi_array< double , 2 > & get_e_cost() const {
  return ( v_ECost );
 }

/*--------------------------------------------------------------------------*/
/// returns the matrix of SlackTardinessBound
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the SlackTardinessBound variable for period h of each
 * customer v.
 * */
 const boost::multi_array< double , 2 > & get_slack_tardiness_bound() const {
  return ( v_slack_tardiness_bound );
 }
/*--------------------------------------------------------------------------*/
/// returns the matrix of SlackEarlinessBound
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the SlackEarlinessBound variable for period h of each
 * customer v.
 * */
 const boost::multi_array< double , 2 > & get_slack_earliness_bound() const {
  return ( v_slack_earliness_bound );
 }
/*--------------------------------------------------------------------------*/
/// returns the matrix of SlackImporterBound
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the SlackImporterBound variable for period h of each
 * importer i.
 * */
 const boost::multi_array< double , 2 > & get_slack_importer_bound() const {
  return ( v_slack_importer_bound );
 }
  /*--------------------------------------------------------------------------*/
/// returns the matrix of SlackExporterBound
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the SlackExporterBound variable for period h of each
 * exporter e.
 * */
  const boost::multi_array< double , 2 > & get_slack_exporter_bound() const {
   return ( v_slack_exporter_bound );
  }
/**@} ----------------------------------------------------------------------*/
/*-------- METHODS FOR READING THE Variable OF THE TwoSlotsVRPBlock --------*/
/*--------------------------------------------------------------------------*/
/** @name Reading the Variable of the TwoSlotsVRPBlock
 * These methods allow to read the each group of Variable that any
 * TwoSlotsVRPBlock in principle has:
 *
 *  - the integer decision variables;
 *
 *  - the slack tardiness variables;
 *
 *  - the slack earliness variables;
 * @{ */

  /// returns the vector of integer decision variables
  const std::vector< ColVariable > & get_integer_decision( ) const {
   return v_integer_decision;
  }
/*--------------------------------------------------------------------------*/
 /// returns the matrix of slack tardiness variable
 const boost::multi_array< ColVariable , 2 > & get_slack_tardiness( ) const {
  return v_slack_tardiness;
 }
/*--------------------------------------------------------------------------*/
 /// returns the matrix of slack earliness variable
 const boost::multi_array< ColVariable , 2 > & get_slack_earliness( ) const {
  return v_slack_earliness;
 }
/**@} ----------------------------------------------------------------------*/
/*----------------- METHODS FOR SAVING THE TwoSlotsVRPBlock-----------------*/
/*--------------------------------------------------------------------------*/
/** @name Methods for loading, printing & saving the TwoSlotsVRPBlock
 *  @{ */

/// extends Block::serialize( netCDF::NcGroup )
/** Extends Block::serialize( netCDF::NcGroup ) to the specific format of a
 *
 * */

 void serialize( netCDF::NcGroup & group ) const override;

/**@} ----------------------------------------------------------------------*/
/*------------- METHODS FOR INITIALIZING THE TwoSlotsVRPBlock --------------*/
/*--------------------------------------------------------------------------*/

/** @name Handling the data of the TwoSlotsVRPBlock
    @{ */

 void load( std::istream & input ) override {
  throw ( std::logic_error( "TwoSlotsVRPBlock::load() not "
                            "implemented yet") );
 };

/*--------------------------------------------------------------------------*/
/*-------------------- PROTECTED PART OF THE CLASS -------------------------*/
/*--------------------------------------------------------------------------*/

 protected:

/*--------------------------------------------------------------------------*/
/*-------------------- PROTECTED FIELDS OF THE CLASS -----------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------data--------------------------------------*/

 /// the matrix of Alpha
 boost::multi_array< Index, 2 > v_alpha;

 /// the vector of cost
 std::vector< double > v_cost;

 /// the matrix of TCost
 boost::multi_array< double, 2 > v_TCost;

 /// the matrix of ECost
 boost::multi_array< double, 2 > v_ECost;

 /// the matrix of SlackTardinessBound
 boost::multi_array< double, 2 > v_slack_tardiness_bound;

 /// the matrix of SlackEarlinessBound
 boost::multi_array< double, 2 > v_slack_earliness_bound;

 /// the matrix of SlackImporterBound
 boost::multi_array< double, 2 > v_slack_importer_bound;

 /// the matrix of SlackExporterBound
 boost::multi_array< double, 2 > v_slack_exporter_bound;

/*-----------------------------variables------------------------------------*/

  /// the integer decision variable
  std::vector< ColVariable > v_integer_decision;

  /// the slack tardiness variable
  boost::multi_array< ColVariable , 2 > v_slack_tardiness;

  /// the slack earliness variable
  boost::multi_array< ColVariable , 2 > v_slack_earliness;

/*----------------------------constraints-----------------------------------*/

/*--------------------------------------------------------------------------*/
/*----------------------- PRIVATE PART OF THE CLASS ------------------------*/
/*--------------------------------------------------------------------------*/
 private:

/*--------------------------------------------------------------------------*/
/*--------------------------- PRIVATE FIELDS -------------------------------*/
/*--------------------------------------------------------------------------*/

 SMSpp_insert_in_factory_h;

/*--------------------------------------------------------------------------*/
/*-------------------------- PRIVATE METHODS -------------------------------*/
/*--------------------------------------------------------------------------*/

};  // end( class( TwoSlotsVRPBlock ) )

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

}  // end( namespace SMSpp_di_unipi_it )

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

#endif /* TwoSlotsVRPBlock.h included */

/*--------------------------------------------------------------------------*/
/*--------------------- End File TwoSlotsVRPBlock.h ------------------------*/
/*--------------------------------------------------------------------------*/
