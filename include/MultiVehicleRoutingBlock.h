/*--------------------------------------------------------------------------*/
/*------------------- File MultiVehicleRoutingBlock.h ----------------------*/
/*--------------------------------------------------------------------------*/
/** @file
 *
 * Header file for the class MultiVehicleRoutingBlock, which implements the ...
 *
 * \version 0.20
 *
 * \date 7 - 06 - 2021
 *
 * \author Antonio Frangioni \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 * \author Ali Ghezelsoflu \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 * \copyright &copy; Antonio Frangioni, and Ali Ghezelsoflu
 */
/*--------------------------------------------------------------------------*/
/*----------------------------- DEFINITIONS --------------------------------*/
/*--------------------------------------------------------------------------*/

#ifndef __MultiVehicleRoutingBlock
#define __MultiVehicleRoutingBlock  \
                     /* self-identification: #endif at the end of the file */

/*--------------------------------------------------------------------------*/
/*------------------------------ INCLUDES ----------------------------------*/
/*--------------------------------------------------------------------------*/

#include "Block.h"

#include "FRowConstraint.h"

#include "VehicleRoutingBlock.h"

/*--------------------------------------------------------------------------*/
/*--------------------------- NAMESPACE ------------------------------------*/
/*--------------------------------------------------------------------------*/

namespace SMSpp_di_unipi_it {

/*--------------------------------------------------------------------------*/
/*-------------------- CLASS MultiVehicleRoutingBlock ----------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------- GENERAL NOTES --------------------------------*/
/*--------------------------------------------------------------------------*/

/// Implementation of the Block concept for the ...
/** //TODO
 */

class MultiVehicleRoutingBlock : public Block {

/*--------------------------------------------------------------------------*/
/*----------------------- PUBLIC PART OF THE CLASS -------------------------*/
/*--------------------------------------------------------------------------*/

 public:

/*--------------------------------------------------------------------------*/
/*--------------------- CONSTRUCTOR AND DESTRUCTOR -------------------------*/
/*--------------------------------------------------------------------------*/
/** @name Constructor and Destructor
 *  @{ */

 /// Constructor of MultiVehicleRoutingBlock, taking possibly a pointer of
 /// its father Block

 explicit MultiVehicleRoutingBlock( Block * father = nullptr ) :
                                                           Block( father ) {}

/*--------------------------------------------------------------------------*/
 /// Destructor of MultiVehicleRoutingBlock

 virtual ~MultiVehicleRoutingBlock() override;

/**@} ----------------------------------------------------------------------*/
/*-------------------------- OTHER INITIALIZATIONS -------------------------*/
/*--------------------------------------------------------------------------*/
/** @name Other initializations
 *  @{ */

/// Extends Block::deserialize( netCDF::NcGroup )
/** Extends Block::deserialize( netCDF::NcGroup ) to the specific format of
 *  the MultiVehicleRoutingBlock. Besides the mandatory "type" attribute of
 *  any :Block, the group should contain the following: //TODO
 *
 * - the dimension "NumberPorts" containing the number of Ports. This
 *   dimension is optional, if it is not provided then it is taken to be == 1.
 *
 * - the dimension "NumberNode" containing the number of Nodes.
 *
 * - the dimension "NumberRoutes" containing the number of all feasible
 *   routes.
 *
 * - the dimension "NumberPeriods" containing the number of periods. This
 *   dimension is optional, if it is not provided then it is taken to be == 1.
 *
 * - The variable "RowDemand", of type double and indexed both over the
 *   dimensions "NumberPeriods" and "NumberCustomers": This is meant to
 *   represent the entry RowDemand[ h , v ] is assumed to contain the demand
 *   of each period h and each customer v, i.e., the number of containers
 *   requested by customer v in period h. Out of this, the "Demand" for
 *   period h of customer v is constructed by summing all of the demand of
 *   day h plus all demands of days before day h.
  */

 void deserialize( const netCDF::NcGroup & group ) override;

/*--------------------------------------------------------------------------*/
/// Generates the static constraint of the MultiVehicleRoutingBlock
/** This method generates the abstract constraints of the
 * MultiVehicleRoutingBlock.
 *
 */

 void generate_abstract_constraints( Configuration * stcc = nullptr ) override;

/**@} ----------------------------------------------------------------------*/
/*------ METHODS FOR READING THE DATA OF THE MultiVehicleRoutingBlock ------*/
/*--------------------------------------------------------------------------*/
/** @name Reading the data of the MultiVehicleRoutingBlock
 *
 * These methods allow to read data that must be common to (in principle) all
 * the related blocks to the UC problem.
 * @{ */

/**@} ----------------------------------------------------------------------*/
/*------------- METHODS FOR SAVING THE MultiVehicleRoutingBlock ------------*/
/*--------------------------------------------------------------------------*/
/** @name Methods for loading, printing and saving the
 * MultiVehicleRoutingBlock
 *  @{ */

 /// Extends Block::serialize( netCDF::NcGroup )
 /** Extends Block::serialize( netCDF::NcGroup ) to the specific format of a
  * MultiVehicleRoutingBlock. See
  * MultiVehicleRoutingBlock::deserialize( netCDF::NcGroup ) for details of
  * the format of the created netCDF group. */

 void serialize( netCDF::NcGroup & group ) const override;

/**@} ----------------------------------------------------------------------*/
/*--------- METHODS FOR INITIALIZING THE MultiVehicleRoutingBlock ----------*/
/*--------------------------------------------------------------------------*/
/** @name Handling the data of the MultiVehicleRoutingBlock
    @{ */

/**
 * @brief It loads a MultiVehicleRoutingBlock from a input standard stream.
 * @warning This method is not implemented yet.
 * @param input an input stream
 */

 void load( std::istream & input ) override {
  throw ( std::logic_error( "MultiVehicleRoutingBlock::load() not implemented"
                                                                    "yet" ) );
 }

/**@} ----------------------------------------------------------------------*/
/*------------------------ METHODS FOR CHANGING DATA -----------------------*/
/*--------------------------------------------------------------------------*/

 /// returns the number of ports
 Index get_number_ports() const { return f_number_ports; }

 /// returns the number of customers
 Index get_number_customers() const { return f_number_customers; }

 /// returns the number of nodes
 Index get_number_nodes() const { return f_number_nodes; }

 /// returns the number of routes
 Index get_number_routes() const { return f_number_routes; }

 /// returns the number of periods
 Index get_number_periods() const { return f_number_periods; }

 /*--------------------------------------------------------------------------*/
/// returns the matrix of RowDemand
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the RowDemand variable for period h of each customer v.
 * */
 const boost::multi_array< double , 2 > & get_row_demand() const {
  return ( v_row_demand );
 }
/*--------------------------------------------------------------------------*/
/// returns the matrix of Demand
/** The method returned a two-dimensional boost::multi_array<> M such that
 * M[ h , v ] gives the Demand variable for period h of each customer v.
 * */
 const boost::multi_array< double , 2 > & get_demand() const {
  return ( v_demand );
 }

/**@} ----------------------------------------------------------------------*/
/*-------------------- PROTECTED FIELDS OF THE CLASS -----------------------*/
/*--------------------------------------------------------------------------*/

 protected:

/*--------------------------------------------------------------------------*/
/*-------------------- PROTECTED METHODS OF THE CLASS ----------------------*/
/*--------------------------------------------------------------------------*/

 /// the number of ports
 Index f_number_ports;

 /// the number of customers
 Index f_number_customers;

 /// the number of nodes
 Index f_number_nodes;

 /// the number of routes
 Index f_number_routes;

 /// the number of periods
 Index f_number_periods;

 /// the matrix of RowDemand
 boost::multi_array< double, 2 > v_row_demand;

 /// the matrix of Demand
 boost::multi_array< double, 2 > v_demand;

/*--------------------------------------------------------------------------*/
/*--------------------- PRIVATE PART OF THE CLASS --------------------------*/
/*--------------------------------------------------------------------------*/

 private:

/*--------------------------------------------------------------------------*/
/*--------------------------- PRIVATE FIELDS -------------------------------*/
/*--------------------------------------------------------------------------*/

 SMSpp_insert_in_factory_h;

/*--------------------------------------------------------------------------*/
/*-------------------------- PRIVATE METHODS -------------------------------*/
/*--------------------------------------------------------------------------*/

};   // end( class( MultiVehicleRoutingBlock ) )

/*--------------------------------------------------------------------------*/

} /* namespace SMSpp_di_unipi_it */

/*--------------------------------------------------------------------------*/

#endif /* MultiVehicleRoutingBlock.h included */

/*--------------------------------------------------------------------------*/
/*------------------ End File MultiVehicleRoutingBlock.h -------------------*/
/*--------------------------------------------------------------------------*/
