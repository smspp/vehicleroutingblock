/*--------------------------------------------------------------------------*/
/*----------------- File MultiVehicleRoutingBlock.cpp ----------------------*/
/*--------------------------------------------------------------------------*/
/** @file
 * Implementation of the MultiVehicleRoutingBlock class.
 *
 * \version 0.1
 *
 * \date 10 - 04 - 2021
 *
 * \author Antonio Frangioni \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * \author Ali Ghezelsoflu \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * Copyright &copy by Antonio Frangioni, Ali Ghezelsoflu
 */

/*--------------------------------------------------------------------------*/
/*---------------------------- IMPLEMENTATION ------------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*------------------------------ INCLUDES ----------------------------------*/
/*--------------------------------------------------------------------------*/
#include "LinearFunction.h"
#include "VehicleRoutingBlock.h"
#include "MultiVehicleRoutingBlock.h"

/*--------------------------------------------------------------------------*/
/*------------------------- NAMESPACE AND USING ----------------------------*/
/*--------------------------------------------------------------------------*/

using namespace SMSpp_di_unipi_it;


/*--------------------------------------------------------------------------*/
/*----------------------------- STATIC MEMBERS -----------------------------*/
/*--------------------------------------------------------------------------*/

// register MultiVehicleRoutingBlock to the Block factory


SMSpp_insert_in_factory_cpp_1( MultiVehicleRoutingBlock );

/*--------------------------------------------------------------------------*/
/*------------------- METHODS OF MultiVehicleRoutingBlock ------------------*/
/*--------------------------------------------------------------------------*/

//TODO

/*--------------------------------------------------------------------------*/
/*-------------------------- OTHER INITIALIZATIONS -------------------------*/
/*--------------------------------------------------------------------------*/

void MultiVehicleRoutingBlock::deserialize( const netCDF::NcGroup & group ) {
 //TODO
}// end( TwoSlotsVRPBlock::deserialize )

/*--------------------------------------------------------------------------*/

void MultiVehicleRoutingBlock::generate_abstract_variables( Configuration *stvv ) {

} // end( MultiVehicleRoutingBlock::generate_abstract_variables )

/*--------------------------------------------------------------------------*/

void MultiVehicleRoutingBlock::generate_abstract_constraints
        ( Configuration *stcc ) {

} // end( MultiVehicleRoutingBlock::generate_abstract_constraints )

/*--------------------------------------------------------------------------*/
/*--- METHODS FOR LOADING, PRINTING & SAVING THE MultiVehicleRoutingBlock --*/
/*--------------------------------------------------------------------------*/

void MultiVehicleRoutingBlock::serialize( netCDF::NcGroup & group ) const {

}  // end( MultiVehicleRoutingBlock::serialize )

/*--------------------------------------------------------------------------*/
/*------------------ End File MultiVehicleRoutingBlock.cpp -----------------*/
/*--------------------------------------------------------------------------*/
