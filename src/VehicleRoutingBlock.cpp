/*--------------------------------------------------------------------------*/
/*------------------- File VehicleRoutingBlock.cpp -------------------------*/
/*--------------------------------------------------------------------------*/
/** @file
 * Implementation of the VehicleRoutingBlock class.
 *
 * \version 0.1
 *
 * \date 10 - 04 - 2021
 *
 * \author Antonio Frangioni \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * \author Ali Ghezelsoflu \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * Copyright &copy by Antonio Frangioni, Ali Ghezelsoflu
 */

/*--------------------------------------------------------------------------*/
/*---------------------------- IMPLEMENTATION ------------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*------------------------------ INCLUDES ----------------------------------*/
/*--------------------------------------------------------------------------*/

#include "VehicleRoutingBlock.h"
#include "MultiVehicleRoutingBlock.h"

/*--------------------------------------------------------------------------*/
/*------------------------- NAMESPACE AND USING ----------------------------*/
/*--------------------------------------------------------------------------*/

using namespace SMSpp_di_unipi_it;


/*--------------------------------------------------------------------------*/
/*----------------------------- STATIC MEMBERS -----------------------------*/
/*--------------------------------------------------------------------------*/

// register VehicleRoutingBlock to the Block factory


SMSpp_insert_in_factory_cpp_1( VehicleRoutingBlock );

/*--------------------------------------------------------------------------*/
/*---------------------- METHODS OF VehicleRoutingBlock --------------------*/
/*--------------------------------------------------------------------------*/

//TODO

/*--------------------------------------------------------------------------*/
/*-------------------------- OTHER INITIALIZATIONS -------------------------*/
/*--------------------------------------------------------------------------*/

void VehicleRoutingBlock::deserialize( const netCDF::NcGroup & group ) {
 //TODO
}// end( TwoSlotsVRPBlock::deserialize )

/*--------------------------------------------------------------------------*/

void VehicleRoutingBlock::generate_abstract_variables( Configuration *stvv ) {

} // end( VehicleRoutingBlock::generate_abstract_variables )

/*--------------------------------------------------------------------------*/

void VehicleRoutingBlock::generate_abstract_constraints
        ( Configuration *stcc ) {

} // end( VehicleRoutingBlock::generate_abstract_constraints )

/*--------------------------------------------------------------------------*/
/*------ METHODS FOR LOADING, PRINTING & SAVING THE VehicleRoutingBlock ----*/
/*--------------------------------------------------------------------------*/

void VehicleRoutingBlock::serialize( netCDF::NcGroup & group ) const {

}  // end( VehicleRoutingBlock::serialize )

/*--------------------------------------------------------------------------*/
/*--------------------- End File VehicleRoutingBlock.cpp -------------------*/
/*--------------------------------------------------------------------------*/
