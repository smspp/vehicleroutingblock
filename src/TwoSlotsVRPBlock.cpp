/*--------------------------------------------------------------------------*/
/*--------------------- File TwoSlotsVRPBlock.cpp --------------------------*/
/*--------------------------------------------------------------------------*/
/** @file
 * Implementation of the TwoSlotsVRPBlock class.
 *
 * \version 0.1
 *
 * \date 10 - 04 - 2021
 *
 * \author Antonio Frangioni \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * \author Ali Ghezelsoflu \n
 *         Operations Research Group \n
 *         Dipartimento di Informatica \n
 *         Universita' di Pisa \n
 *
 *
 * Copyright &copy by Antonio Frangioni, Ali Ghezelsoflu
 */

/*--------------------------------------------------------------------------*/
/*---------------------------- IMPLEMENTATION ------------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*------------------------------ INCLUDES ----------------------------------*/
/*--------------------------------------------------------------------------*/

#include <iostream>
#include <random>
#include "LinearFunction.h"
#include "FRealObjective.h"
#include <map>
#include "TwoSlotsVRPBlock.h"

/*--------------------------------------------------------------------------*/
/*------------------------- NAMESPACE AND USING ----------------------------*/
/*--------------------------------------------------------------------------*/

using namespace SMSpp_di_unipi_it;


/*--------------------------------------------------------------------------*/
/*----------------------------- STATIC MEMBERS -----------------------------*/
/*--------------------------------------------------------------------------*/

// register TwoSlotsVRPBlock to the Block factory


SMSpp_insert_in_factory_cpp_1( TwoSlotsVRPBlock );

/*--------------------------------------------------------------------------*/
/*------------------------ METHODS OF TwoSlotsVRPBlock ---------------------*/
/*--------------------------------------------------------------------------*/

//TODO

/*--------------------------------------------------------------------------*/
/*-------------------------- OTHER INITIALIZATIONS -------------------------*/
/*--------------------------------------------------------------------------*/

void TwoSlotsVRPBlock::deserialize( const netCDF::NcGroup & group ) {
 //TODO
}// end( TwoSlotsVRPBlock::deserialize )

/*--------------------------------------------------------------------------*/

void TwoSlotsVRPBlock::generate_abstract_variables( Configuration *stvv ) {

} // end( TwoSlotsVRPBlock::generate_abstract_variables )

/*--------------------------------------------------------------------------*/

void TwoSlotsVRPBlock::generate_abstract_constraints
        ( Configuration *stcc ) {

} // end( TwoSlotsVRPBlock::generate_abstract_constraints )

/*--------------------------------------------------------------------------*/

void TwoSlotsVRPBlock::generate_objective( Configuration * objc ) {

}  // end( TwoSlotsVRPBlock::generate_objective )
/*--------------------------------------------------------------------------*/
/*------- METHODS FOR LOADING, PRINTING & SAVING THE TwoSlotsVRPBlock ------*/
/*--------------------------------------------------------------------------*/

void TwoSlotsVRPBlock::serialize( netCDF::NcGroup & group ) const {

}  // end( TwoSlotsVRPBlock::serialize )

/*--------------------------------------------------------------------------*/
/*--------------------- End File TwoSlotsVRPBlock.cpp ----------------------*/
/*--------------------------------------------------------------------------*/
